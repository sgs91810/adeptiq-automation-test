* Quick summary: Test Automation Scripts for Adept IQ (2.7.0 and later Release version)
Version1: Test Scenario:
Most of Search Booking Scenario (NYAAR-7804, NYAAR-10913)
Create Booking: Taxi and Waitlisted Trip Status - Integration Test which include (NYAAR-7279)
Profile Check: NYAAR-7277 and NYAAR-7255 (one scenario)

* Version: Node Version: 12.4.0

* Summary of set up: yarn install

* How to run tests
By Default, running on the localhost environment.

Run with Cypress Open - debug and develop scripts
yarn cypress open --env configFile=aiqtan2 --browser chrome

Test on aiqtan2:
yarn cypress run --env configFile=aiqtan2 --browser chrome

Run Entire Test Script:
Run Locally: Must get latest code from the iqux-ui repo (git pull)
and Run on the latest code by executing following command: yarn run cypress:e2e

AIQ Cyan2: yarn run cypress:cyan2
AIQ Tan2: yarn run cypress:tan2

Smoke Testing Run:
yarn cypress run --env configFile=aiqtan2,grep="smoke" --browser chrome
yarn cypress run --env configFile=aiqtan2,grepTags=@smokeTag --browser chrome

Cyan2: yarn run cypress:cyan2Smoke
Tan2: yarn run cypress:tan2Smoke


Original Repo Owner: Chong S. Lee
