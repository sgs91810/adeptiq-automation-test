FROM cypress/base:12.4.0
RUN mkdir /cy_app
WORKDIR /cy_app
COPY . /cy_app

RUN echo '@dds:registry=https://npm.aiqdev.net/ \n\
email=jenkins@ddswireless.com \n\
strict-ssl=false \n\
//npm.aiqdev.net/:_authToken="MAAqAEfCcmO7u/KlYA4/ig=="' >> ~/.npmrc

RUN yarn install
RUN yarn add bower
RUN yarn global add start-server-and-test

RUN yarn cypress verify
CMD ["yarn","run", "cypress:e2e"]