/// <reference types="cypress" />

export class StringInfo {
    capitalizeFirstLetter = ([first,...rest]) => first.toUpperCase() + rest.join('').toLowerCase();


}

export const stringInfo = new StringInfo()