/// <reference types="cypress" />

export class TripInfo {

    tripReqWait(token, bookingId, scheduleId) {
        let totalWait =0
        /*cy.request({  // #4
            method: 'GET',
            url: Cypress.env('apiUrl')+'scheduling/trip',
            qs: {
                filter: `and(eq(bookingId,'${bookingId}'),eq(scheduleId,'${scheduleId}'))`
            },
            headers: {
                accept:"application/json",
                authorization:'Bearer '+token
            }
        }) */

        cy.api_scheduleTrip(token,bookingId, scheduleId)

        .then((resp) => {
            if (resp.status === 200 && resp.body.data.length>0) {
                cy.log(resp.body)
                cy.log('Got Trip Data')
                return
            }

            else {
                cy.log('No Trip Data - Wait 1.5sec')
                cy.wait(1500)
                totalWait += 1500
                if (totalWait > 60000) {
                    return false
                } else {
                    this.tripReqWait(token,bookingId,scheduleId)
                }
            }
        })
    }

}

export const tripInfo = new TripInfo()