/// <reference types="cypress" />
import { dateTimeInfo } from "./DateTimeInfo"
const moment = require('moment')
const { _ } = Cypress

export class BookingCreationInfo {

    getBookingAddressInfo(bookingArray, segmentStopArray,segStopType) {
        const addressObj = {}
        const segStopIndex = bookingArray.findIndex( ({type,attributes}) => type ==='segmentStop' && attributes.type == segStopType);
        addressObj.stopIndex = segStopIndex;
        addressObj.freeformAddress = segmentStopArray[segStopIndex].relationships.address.data.freeformAddress
        addressObj.address = segmentStopArray[segStopIndex].relationships.address.data.streetNumber+" "+
        segmentStopArray[segStopIndex].relationships.address.data.streetAddress+" "+
        segmentStopArray[segStopIndex].relationships.address.data.premise
        addressObj.city = segmentStopArray[segStopIndex].relationships.address.data.locality
        return addressObj
    }

    createBookingSchedule(token, bookingFixture, bookingTable, tripTable) {
        // temporary
        const tomorrow = dateTimeInfo.getDateTime(9,30,86400000),
        primaryDateTomorrow = moment(tomorrow).format('YYYY-MM-DD'),
        bookingDateTomorrow =primaryDateTomorrow+'B',
        tomorrowDateFormat = moment(tomorrow).format('YYYY-MM-DD'),
        tomorrowTimeFormat = moment(tomorrow).format('hh:mm A')

        const bookingArray = bookingFixture.included
        const segmentStopArray = bookingArray.filter( ({type}) => type ==='segmentStop' );
        const segStopPickIndex = bookingArray.findIndex( ({type,attributes}) => type ==='segmentStop' && attributes.type == 'pick');
        const pickAddress = segmentStopArray[segStopPickIndex].relationships.address.data.freeformAddress
        const segStopDropIndex = bookingArray.findIndex( ({type,attributes}) => type ==='segmentStop' && attributes.type == 'drop');
        const dropAddress = segmentStopArray[segStopDropIndex].relationships.address.data.freeformAddress

        const segment = bookingArray.find( ({type}) => type ==='segment' );
        const anchor = segment.attributes.anchor
        segment.attributes.promiseTime = tomorrow.toISOString()
        tripTable.tripDateRequest = bookingTable.tripDateRequest = tomorrowDateFormat.concat(" / ",anchor," / ",tomorrowTimeFormat)

        const leg = bookingArray.find( ({type}) => type ==='leg');
        leg.attributes.requestTime = tomorrow.toISOString()

        tripTable.riderId = bookingTable.riderID = leg.relationships.rider.data.riderId
        tripTable.firstName = bookingTable.firstName = leg.relationships.rider.data.firstName
        tripTable.lastName = bookingTable.lastName =  leg.relationships.rider.data.lastName
        tripTable.pickAddress = bookingTable.pickAddress = segmentStopArray[0].relationships.address.data.streetNumber+" "+
                    segmentStopArray[0].relationships.address.data.streetAddress+" "+
                    segmentStopArray[0].relationships.address.data.premise

        tripTable.pickCity = bookingTable.pickCity = segmentStopArray[0].relationships.address.data.locality
        tripTable.dropAddress = bookingTable.dropAddress = segmentStopArray[1].relationships.address.data.streetNumber+" "+
                    segmentStopArray[1].relationships.address.data.streetAddress+" "+
                    segmentStopArray[1].relationships.address.data.premise

        tripTable.dropCity = bookingTable.dropCity = segmentStopArray[1].relationships.address.data.locality



        bookingTable.groupBooking = "No"

        cy.api_Geocode(token,pickAddress)
        cy.then(()=>{
            expect(this.getGeocode.status).to.equal(200)
            segmentStopArray[segStopPickIndex].relationships.location.data.geoNode = this.getGeocode.body.data[0].geonode
            cy.api_Geocode(token,dropAddress)
        })
    }
}

export const bookingCreationInfo = new BookingCreationInfo()