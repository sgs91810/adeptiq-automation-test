/// <reference types="cypress" />
const moment = require('moment')

export class DateTimeInfo {

    // Format: MM/DD/YYYY
    formatDateTwoMonthDayFourYear(payloadDate) {
        const date = payloadDate.split('-')
        const day = date[2].split('T')
        const dateObj = {month: date[1], day: day[0], year: date[0]}

        const formattedDate = dateObj.month+'/'+dateObj.day+'/'+dateObj.year
        return formattedDate
    }

    getDateTime(hr, min, additionTime) {
        let today = new Date()
        today.setHours(hr)
        today.setMinutes(min)
        today.setSeconds(0)
        today.setMilliseconds(0)

        const tomorrow = new Date(today.getTime() + (additionTime));
        return tomorrow
    }

    // For Rider Eligibility
    getTodayISOFormat() {
        const today = moment().format('YYYY-MM-DD')
        const todayISOFormat = today + "T00:00:00"
        return todayISOFormat
    }

    getAfterTomorrowISOFormat() {
        const todayDate = new Date()
        const afterTomorrow = new Date(todayDate.getTime() + (86400000*2));
        const afterTomorrowDateString = moment(afterTomorrow).format('YYYY-MM-DD')
        const afterTomorrowISOFormat = afterTomorrowDateString + "T00:00:00"
        return afterTomorrowISOFormat
    }

}

export const dateTimeInfo = new DateTimeInfo()