Cypress.Commands.add('api_getEligibilityCategory',(token,category) => {
    cy.request({
        method: 'GET',
        url: Cypress.env('apiUrl')+`config/config/config-System_Configuration-eligibility_categories/${category}`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        },
        failOnStatusCode: false
    }).as('getEligibilityCategory')
})

Cypress.Commands.add('api_createEligibilityCategory',(token,categoryFixture) => {
    cy.request({
        method: 'POST',
        url: Cypress.env('apiUrl')+`config/config/config-System_Configuration-eligibility_categories`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        },
        body:categoryFixture
    }).as('createEligibilityCategory')
})

Cypress.Commands.add('api_checkEligibilityCategory',(token,category,categoryFixture)=>{
    cy.api_getEligibilityCategory(token,category)
      .then((getEligibilityResponse)=>{
        const status = getEligibilityResponse.status
        if (status == 404) {
            cy.fixture(categoryFixture).then(categoryFixture => {
                cy.api_createEligibilityCategory(token,categoryFixture)
                .then((createEligibilityResponse)=> {
                    expect(createEligibilityResponse.status).to.equal(200)
                })
            })
        }
    })
})

Cypress.Commands.add('api_getServiceNeedsTypes',(token,serviceNeedType)=>{
    cy.request({
		method:"GET",
		url:Cypress.env('apiUrl')+`config/config/config-System_Configuration-service_needs_types/${serviceNeedType}`,
		headers: {
			accept:"application/json",
			authorization:'Bearer '+token
        },
        failOnStatusCode: false
    })
})

Cypress.Commands.add('api_postServiceNeedsTypes',(token,serviceNeedFixture)=>{
    cy.request({
		method:"POST",
		url:Cypress.env('apiUrl')+'config/config/config-System_Configuration-service_needs_types/',
		headers: {
			accept:"application/json",
			authorization:'Bearer '+token
		},
        body:serviceNeedFixture
    })
})

Cypress.Commands.add('api_checkserviceNeedsTypes',(token,serviceNeedType,serviceNeedFixture)=>{
    cy.api_getServiceNeedsTypes(token,serviceNeedType)
      .then( (getServiceNeedResponse)=>{
        const status = getServiceNeedResponse.status
        if (status == 404) {
            cy.fixture(serviceNeedFixture).then(serviceNeedFixture => {
                cy.api_postServiceNeedsTypes(token,serviceNeedFixture)
                .then((createServiceNeedResponse)=> {
                    expect(createServiceNeedResponse.status).to.equal(200)
                })
            })
        }
    })
})

Cypress.Commands.add('api_getConfiguration',(token,configType)=>{
    cy.request({
        method:"GET",
        url:Cypress.env('apiUrl')+`config/config/${configType}`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        }
    })
})

Cypress.Commands.add('api_patchConfiguration',(token,configType,fixture)=>{
    cy.request({
        method:"PATCH",
        url:Cypress.env('apiUrl')+`config/config/${configType}`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        },
        body:fixture
    })
})

Cypress.Commands.add('api_checkPatchConfigType',(token,configType,fixture)=>{
    cy.api_getConfiguration(token,configType)
      .then((getConfigResponse)=>{
        const status = getConfigResponse.status
        if (status == 404) {
            cy.fixture(fixture).then(fixture => {
                cy.api_patchConfiguration(token,configType,fixture)
                .then((patchConfigResponse)=> {
                    expect(patchConfigResponse.status).to.equal(200)
                })
            })
        }
    })
})