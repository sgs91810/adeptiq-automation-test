Cypress.Commands.add('getErrorNotification',(errorMsg)=>{
    cy.get('.notification-bar').within(($alert) => {
        cy.get('.alert-text').should('contain.text',errorMsg)
        //cy.get('.notification-bar .alert-warning').should('have.css', 'background-color', 'rgb(227, 97, 25)')
      })
});