Cypress.Commands.add('displaySpinnerNoAPIWait',()=>{
    cy.get('.application-wrapper > .global-spinner')
      .as('spinner')
      .should('be.visible')

    cy.get('@spinner')
      .should('not.exist')
});

Cypress.Commands.add('displaySpinnerAPIWait',(interceptedApi)=>{
    cy.get('.application-wrapper > .global-spinner')
    .as('spinner')
    .should('be.visible')

    cy.wait(interceptedApi)

   cy.get('@spinner')
     .should('not.exist')
});