// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('loginFromUI', () => {

    Cypress.log({
        message: 'UI Log In',
        displayName: 'loginFromUI',
        name: 'Login From UI'
    });

    const userCredential = {
        "user": {
            "email": Cypress.env('username'),
            "password": Cypress.env('password')
         }
    }

    cy.visit('/login')
    cy.get('input[data-test-email-input').should('be.visible').clear()
    cy.get('input[data-test-email-input').type(userCredential.user.email)
    cy.get('input[data-test-password-input]').should('be.visible').clear()
    cy.get('input[data-test-password-input]').type(userCredential.user.password)
    cy.get('form').submit()
});

Cypress.Commands.add('getToken',()=>{

    Cypress.log({
        message: 'Get Token',
        displayName: 'getToken',
        name: 'Get Token With API'
    });

    const userPayload = {
        "plugin": "internal;adept4;basic",
        "internal": {
            "email": Cypress.env('username'),
            "password": Cypress.env('password')
        },
        "basic": {
            "username": Cypress.env('username'),
            "password": Cypress.env('password')
        },
        "adept4": {
            "username": Cypress.env('username'),
            "password": Cypress.env('password')
        }
    };

    cy.request({
        method: 'POST',
        url: Cypress.env('apiUrl')+'sso/auth-token',
        body:userPayload
    }).as('token')

    // Need delete all cookies before start
})


Cypress.Commands.add('loginWithAPI', () => {

    Cypress.log({
        message: 'API Log In',
        displayName: 'loginWithAPI',
        name: 'Login With API'
    });

    const userCredential = {
        "user": {
            "id": Cypress.env('userId'),
            "email": Cypress.env('username'),
            "password": Cypress.env('password')
         }
    }

    const userPayload = {
        "plugin": "internal;adept4;basic",
        "internal": {
            "email": Cypress.env('username'),
            "password": Cypress.env('password')
        },
        "basic": {
            "username": Cypress.env('username'),
            "password": Cypress.env('password')
        },
        "adept4": {
            "username": Cypress.env('username'),
            "password": Cypress.env('password')
        }
    }

   const timeIat = Math.round((new Date()).getTime() / 1000);
   const timeExp =timeIat+(18*3600)

    cy.request('POST',Cypress.env('apiUrl')+'sso/auth-token',userPayload)
      .its('body').then(body => {

        const token = body.token;
        cy.wrap(token).as('token')

        const authSessionValue = {
            "authenticated": {
                "authenticator": "authenticator:sso",
                "token": token,
                "exp": timeExp,
                "userId": userCredential.user.id,
                "permissions": [0,162,165,163,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,87,88,89,91,92,93,94,95,96,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,82,83,86,158,159,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,160,161,157],
                "tokenInfo": {
                }
            }
        }

        authSessionValue.authenticated.tokenInfo.userId = userCredential.user.id
        authSessionValue.authenticated.tokenInfo.email = userCredential.user.email
        authSessionValue.authenticated.tokenInfo.firstName = "admin"
        authSessionValue.authenticated.tokenInfo.lastName = ""
        authSessionValue.authenticated.tokenInfo.sequenceNumber = 1
        authSessionValue.authenticated.tokenInfo.roleNames = ["admin"]
        authSessionValue.authenticated.tokenInfo.providerNames = ["ALL"]
        authSessionValue.authenticated.tokenInfo.permissions = [0,162,165,163,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,87,88,89,91,92,93,94,95,96,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,82,83,86,158,159,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,160,161,157]
        authSessionValue.authenticated.tokenInfo.iat = timeIat
        authSessionValue.authenticated.tokenInfo.exp = timeExp
        authSessionValue.authenticated.tokenInfo.iss = Cypress.env('apiUrl')

        const jsonAuthSessionValue = JSON.stringify(authSessionValue)

        cy.visit('/', {
            onBeforeLoad(win) {
                win.localStorage.setItem('ember_simple_auth-session',jsonAuthSessionValue)
                win.localStorage.setItem('currentUser',userCredential.user.email)
                win.localStorage.setItem('currentPassword',userCredential.user.password)
                win.localStorage.setItem('currentUserId',userCredential.user.id)

                win.localStorage.setItem('userDash','iq-admin@ddswireless.com*workspaces-booking/booking')
                win.localStorage.setItem('lastDashboardId','workspaces-booking/booking')

            }
        })
    });
});