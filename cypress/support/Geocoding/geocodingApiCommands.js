Cypress.Commands.add('api_Geocode',(token, address) =>
    cy.request({
        method: 'GET',
        url: Cypress.env('apiUrl')+'geocoding/geocode',
        qs: {
            query: `${address}`
        },
        headers: {
            accept:"application/json",
            authorization:`Bearer ${token}`
        }
    }).as('getGeocode')
)