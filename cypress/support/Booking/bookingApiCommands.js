Cypress.Commands.add('api_BookingWithPromiseTime',(token, bookingFixture) =>
    cy.request({
        method: 'POST',
        url: Cypress.env('apiUrl')+'booking/booking-with-promise-time',
        headers: {
            accept:"application/json",
            authorization:`Bearer ${token}`
        },
        body:bookingFixture
    }).as('bookingSchedule')
)

Cypress.Commands.add('api_primaryBooking',(token,primaryFixture) =>
    cy.request({
        method: 'POST',
        url: Cypress.env('apiUrl')+'booking/primary-booking',
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        },
        body:primaryFixture
    }).as('primarySchedule')
)

Cypress.Commands.add('api_bookingId',(token,bookingId) =>
    cy.request({
        method: 'GET',
        url: Cypress.env('apiUrl')+`booking/booking/${bookingId}`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        }
    }).as('bookingID')
)

Cypress.Commands.add('api_deleteBookingOnly',(token,bookingId) =>
    cy.request({
        method: 'DELETE',
        url: Cypress.env('apiUrl')+`booking/booking/${bookingId}`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        },
        body: {"doCancel":true}
    }).as('deleteBookingOnly')
)

Cypress.Commands.add('api_createBookingEntity',(token, willFixture) => {
    cy.request({
        method: 'POST',
        url: Cypress.env('apiUrl')+'booking/booking-with-subentity',
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        },
        body:willFixture
    }).as('noScheduleBooking')
})

Cypress.Commands.add('api_deleteBooking',(token, bookingId) =>
    cy.request({
        method: 'DELETE',
        url: Cypress.env('apiUrl')+`booking/booking-with-subentity/${bookingId}`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        },
        body: {"doCancelTrip":true}
    }).as('deleteBookingTrip')
)
