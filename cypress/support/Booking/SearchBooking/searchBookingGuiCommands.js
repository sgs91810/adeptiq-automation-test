// File location
//import { onSearchPassengerSelectionPage } from "../../../support/PageObjects/Booking/SearchPassengerSelectionPage"
Cypress.Commands.add('clickSearchRiderButton',(interceptedApi)=>{
    cy.contains('button','Search').should('not.be.disabled')
    cy.contains('button','Search').click({force: true})

    cy.get('.application-wrapper > .global-spinner')
      .as('spinner')
      .should('be.visible')

    cy.wait(interceptedApi)

    cy.get('@spinner')
      .should('not.exist')
});

Cypress.Commands.add('selectRiderWaitBookingTable',()=>{

  cy.get('.application-wrapper > .global-spinner')
    .as('spinner')
    .should('be.visible')

  //cy.wait('@getEligibility')
  cy.wait('@getRiderManagment')

  //cy.wait('@getAddressPlace')
  //cy.wait('@getRiderManagment')
  cy.wait('@getBookingSubscription')
  cy.wait('@getAddressPlace')

  cy.get('@spinner')
    .should('not.exist')
});