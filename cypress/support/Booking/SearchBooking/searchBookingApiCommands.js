Cypress.Commands.add('api_searchRider',(token,riderId) =>
    cy.request({
        method:"GET",
        url:Cypress.env('apiUrl')+'booking/rider-external/'+riderId,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        }
    })
)

Cypress.Commands.add('externalRiderInterceptAPI',fixtureData=>{
    cy.intercept('GET','**/booking/rider-external/**',
    {
      delay:1000,
      fixture: fixtureData
    }).as('getDelayBookingRider')
});

Cypress.Commands.add('riderReplyInterceptAPI',fixtureData=>{
    cy.intercept('GET','**/booking/rider-external/**', req => {
        req.reply({
            body: fixtureData,
            delayMs:1000
        })
    }).as('getDelayBookingRider')
});

Cypress.Commands.add('getRiderIDInterceptAPI',(riderID, riderFixture)=>{
    cy.intercept('GET','**/booking/rider-external/'+riderID+'', req => {
        req.reply({
           body: riderFixture
         })
    })
});

Cypress.Commands.add('getRiderNamesInterceptAPI',(status,fName,lName,pNumber,riderFixture)=>{
    cy.intercept('GET',`**/booking/rider-external/ShortVer?&activeOnly=${status}&firstName=${fName}&lastName=${lName}&phoneNumber=${pNumber}`, req => {
        req.reply({
          body: riderFixture,
          delayMs:1000
        })
    })
});

Cypress.Commands.add('getRiderEligibilityReplyInterceptAPI',fixtureData=>{
    cy.intercept('GET','**/eligibility', req => {
        req.reply({
          body: fixtureData,
          delayMs:1000
        })
    })
});


Cypress.Commands.add('selectRiderInterceptBookingTable',()=>{
    cy.intercept('GET','**/rider-management/rider?**').as('getRiderManagment')
    cy.intercept('GET','**/address/place?**').as('getAddressPlace')
    cy.intercept('GET','**/booking/subscription?**').as('getBookingSubscription')
});