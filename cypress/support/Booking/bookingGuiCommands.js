Cypress.Commands.add('bookingLandingWorkspaceInterceptAPIs',()=>{
      // 1st Pass:
      cy.intercept('GET','**/config/config/config-UI_restrictions').as('getUIConfigRestriction')
      cy.intercept('GET','**/config/config/workspaces-booking/booking').as('getConfigBookingWorkspaces')

      cy.intercept('GET','**/fres/configuration/settings').as('getConfigSettings')
      cy.intercept('GET','**/sso/role').as('getRole')

      // 2nd Pass:
      cy.intercept('GET','**/config/config/category').as('getConfigCategory')
      cy.intercept('GET','**/config/config/config-System_Configuration-zones').as('getConfigSystemConfigurationZones')
      cy.intercept('GET','**/config/config/config-Notification_Banners').as('getConfigNotificationBanners')
});

Cypress.Commands.add('bookingLandingWorkspaceWaitAPIs',()=>{
      cy.wait('@getUIConfigRestriction')
      cy.wait('@getConfigBookingWorkspaces')

      cy.wait('@getConfigSettings')
      cy.wait('@getRole')

      // 2nd Pass:
      cy.wait('@getConfigCategory')
      cy.wait('@getConfigSystemConfigurationZones')
      cy.wait('@getConfigNotificationBanners')

});

/*
Cypress.Commands.add('sortingTables',(columnInfo)=>{
   // Sorting Tables

   const passengerColumns = {}
   passengerColumns.ascSortLocator = columnInfo.ascLocator //'.column-controls:first .fa-sort-asc'
   passengerColumns.descSortLocator = columnInfo.descLocator //'.column-controls:first .fa-sort-desc'
   passengerColumns.columnLabelText = columnInfo.labelText  //'Status'

   passengerColumns.firstRowActualDataLocator = columnInfo.firstRowLocator // 'td.status > div:first'
   passengerColumns.lastRowActualDataLocator = columnInfo.lastRowLocator //'td.status > div:last'

   passengerColumns.firstAscRowExpectedData = sortingSearchResults.riders[columnInfo.firstAscIndex].status
   passengerColumns.lastAscRowExpectedData =  sortingSearchResults.riders[columnInfo.lastAscIndex].status
   passengerColumns.firstDescRowExpectedData = sortingSearchResults.riders[columnInfo.firstDescIndex].status
   passengerColumns.lastDescRowExpectedData =  sortingSearchResults.riders[columnInfo.lastDescIndex].status

   //onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
});
*/