Cypress.Commands.add('waitForCreateBookingPage',()=>{
       cy.wait('@getRiderInfo')
       cy.wait('@getEligibility')
       cy.checkUrlLocation('/dashboard/Booking/workspaces-booking%2Fbooking/modals/edit-form')
       cy.contains('.g-side-drawer-header-title','Create Booking').should('be.visible')
  });