Cypress.Commands.add('checkUrlLocation',(location)=>{
    cy.location({defaultCommandTimeout: 30000}).should((loc) => {
        expect(loc.pathname).to.eq(location)
        // '/dashboard/Booking/workspaces-booking%2Fbooking/modals/edit-form'
    })
});