Cypress.Commands.add('checkFilterSettings',(dataFilter)=>{
    cy.get(dataFilter).should('be.visible').click();
    cy.get('h1.g-side-drawer-header-title').should('contain.text','Filter Settings')
    //cy.get('.data-test-filter-side-bar-table .filter-settings-table')
    cy.get('.display-checkbox')
      .as('checkboxes')

    cy.get('@checkboxes')
      .each(checkbox => {
          const initial = Boolean(checkbox.prop('checked'))
          //cy.log(`Initial Checkbox: ${initial}`)
          if (!initial) cy.get('@checkbox').check()
      })
    cy.get('[title="Close"]').click()
});