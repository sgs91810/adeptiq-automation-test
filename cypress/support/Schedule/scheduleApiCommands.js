Cypress.Commands.add('api_schedule',(token,dateType) =>
    cy.request({
        method: 'GET',
        url: Cypress.env('apiUrl')+`scheduling/schedule?filter=eq(name,${dateType})`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        }
    }).as('schedule')
)

Cypress.Commands.add('api_scheduleAynscOperation',(token,scheduleAsynOpId) =>
    cy.request({
        method: 'GET',
        url: Cypress.env('apiUrl')+`scheduling/schedule-async-operation/${scheduleAsynOpId}`,
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        }
    }).as('scheduleAsyncOperation')
)

/**
 * JSDoc: api_scheduleTrip
 */
Cypress.Commands.add('api_scheduleTrip',(token,bookingId, scheduleId) =>
    cy.request({
        method: 'GET',
        url: Cypress.env('apiUrl')+'scheduling/trip',
        qs: {
            filter: `and(eq(bookingId,'${bookingId}'),eq(scheduleId,'${scheduleId}'))`
        },
        headers: {
            accept:"application/json",
            authorization:'Bearer '+token
        }
    }).as('scheduleTrip')
)