/// <reference types="cypress" />

export function waitForSpinnerDisapper() {
    cy.get('.application-wrapper > .global-spinner')
    cy.get('.application-wrapper > .global-spinner').should('not.exist')
}

// temp function - move to utils folder
export function goToBookingWorkspace() {
    //cy.visit('/dashboard/Booking/workspaces-booking%2Fbooking')
}