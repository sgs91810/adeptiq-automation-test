/// <reference types="cypress" />

export class PlaybackPage {
    getHeaderName() {
        cy.get('.playback-widget-title', { timeout: 7000}).contains('Playback')
    }
}