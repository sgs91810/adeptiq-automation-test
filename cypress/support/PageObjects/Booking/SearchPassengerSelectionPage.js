/// <reference types="cypress" />
import {
    waitForSpinnerDisapper
} from '../Global/GlobalPage'
const { _ } = Cypress

export class SearchPassengerSelectionPage {

uncheckActiveOnly() {
    cy.get('[class="ember-checkbox ember-view"]').uncheck()
}

checkInputGreenBorderColor() {
//  cy.get('.booking-dashboard .booking-wrapper .fld-sec input:focus')
    cy.get('.booking-dashboard .booking-wrapper .fld-sec input')
      .focused().then(($el)=>{
        expect($el).to.have.css('outline-color','rgb(99, 201, 21)')
    })
}

checkButtonGreenBorderColor() {
    cy.get('.booking-dashboard .booking-wrapper .btn:focus')
      .focused().then(($el)=>{
        expect($el).to.have.css('outline-color','rgb(99, 201, 21)')
    })
}

enterRiderID(idValue) {
    cy.get('input[placeholder="ID Number"]').should('be.visible').clear()

    cy.get('input[placeholder="ID Number"]').focus()
    cy.get('input[placeholder="ID Number"]').type(idValue)
      .should('have.value',idValue)
}

enterFirstName(firstNameValue) {
    cy.get('input[placeholder="First Name"]').should('be.visible').clear()
    cy.get('input[placeholder="First Name"]').focus()
    cy.get('input[placeholder="First Name"]').type(firstNameValue)
      .should('have.value',firstNameValue)
}

enterLastName(lastNameValue) {
    cy.get('input[placeholder="Last Name"]').should('be.visible').clear()
    cy.get('input[placeholder="Last Name"]').focus()
    cy.get('input[placeholder="Last Name"]').type(lastNameValue)
      .should('have.value',lastNameValue)
}

enterPhoneNumber(phoneValue) {
    cy.get('input[placeholder="000 000 0000"]').should('be.visible').clear()
    cy.get('input[placeholder="000 000 0000"]').focus()
    cy.get('input[placeholder="000 000 0000"]').type(phoneValue)
      .should('have.value',phoneValue)
}

enabledActiveCheckbox() {
    cy.get('[class="ember-checkbox ember-view"]')
      .should('be.visible').and('be.checked')
      .focus()
}

clickSearchButton() {
    cy.contains('button','Search').should('not.be.disabled')
    cy.contains('button','Search').click({force: true})
}

clickClearButton() {
    cy.contains('button','Clear').should('not.be.disabled').and('be.visible')
    cy.contains('button','Clear').click({force: true})
}

checkWorkspaceLogo() {
    //cy.contains('.workspace-logo','Booking - booking').should('be.exist') // for booking agent
}

checkSearchPassengerHeading() {-
    cy.contains('.sp-heading', { timeout: 10000},'Search Passenger')
}

disabledIdField() {
    cy.get('input[placeholder="ID Number"]').should('be.disabled')
}

disabledNamesPhoneFields() {
    cy.get('input[placeholder="First Name"]').should('be.disabled')
    cy.get('input[placeholder="Last Name"]').should('be.disabled')
    cy.get('input[placeholder="000 000 0000"]').should('be.disabled')
}

enterNamesWithManualEnterKey(firstNameValue, lastNameValue) {
    cy.get('input[placeholder="First Name"]').should('be.visible').clear()
    cy.get('input[placeholder="First Name"]').type(firstNameValue)

    cy.get('input[placeholder="Last Name"]').should('be.visible').clear()
    cy.get('input[placeholder="Last Name"]').type(lastNameValue +'{enter}')

    cy.get('input[placeholder="ID Number"]').should('be.disabled')
   // waitForSpinnerDisapper()
}

enabledSearchClearButtons() {
    cy.contains('button','Search').should('be.enabled')
    cy.contains('button','Clear').should('be.enabled')
}

getAlertMessage(warningMsg) {
    cy.get('.alert-text').should('contain',warningMsg)
}

sortSearchPassenger(passengerColumnObj) {
    // 1. Ascending Order:
    cy.get('.passengers-booking-widget table')
    .should('have.length',2)
    .eq(0).then(passengerTableHead => {
          cy.wrap(passengerTableHead).within(() => {  // 1. Ascending Order:
            cy.get(passengerColumnObj.ascSortLocator).should('not.exist')
            cy.contains('.column-label', passengerColumnObj.columnLabelText).click()
            cy.get(passengerColumnObj.ascSortLocator).should('exist')
          })
      })

    cy.get('.passengers-booking-widget table')
      .should('have.length',2)
      .eq(1).then(passengerTableBody => {
          cy.wrap(passengerTableBody).within(() => {  // 1. Ascending Order:
            //cy.get('td.status > div > span:first').should('have.class', 'active-dot')
            //cy.contains(passengerColumnObj.firstRowActualDataLocator,passengerColumnObj.firstAscRowExpectedData)
            cy.log(passengerColumnObj.firstRowActualDataLocator)
            cy.log(passengerColumnObj.lastRowActualDataLocator)
            cy.log(passengerColumnObj.firstAscRowExpectedData)
            cy.log(passengerColumnObj.lastAscRowExpectedData)
            //cy.contains(passengerColumnObj.lastRowActualDataLocator, passengerColumnObj.lastAscRowExpectedData)

          })
      })

    // 2. Descending Order:
    cy.get('.passengers-booking-widget table')
      .should('have.length',2)
      .eq(0).then(passengerTableHead => {
          cy.wrap(passengerTableHead).within(() => {
            cy.get(passengerColumnObj.descSortLocator).should('not.exist')
            cy.contains('.column-label', passengerColumnObj.columnLabelText).click()
            cy.get(passengerColumnObj.descSortLocator).should('exist')
          })
      })

    cy.get('.passengers-booking-widget table')
      .should('have.length',2)
      .eq(1).then(passengerTableBody => {
          cy.wrap(passengerTableBody).within(() => {
            //cy.get('td.status > div > span:first').should('have.class', 'inactive-dot')
            cy.contains(passengerColumnObj.firstRowActualDataLocator,passengerColumnObj.firstDescRowExpectedData)
            cy.contains(passengerColumnObj.lastRowActualDataLocator, passengerColumnObj.lastDescRowExpectedData)
          })
      })
}

// Trip Booking tables:
checkFirstTripTable(tripTable) {
  cy.get('.view-alltrips-widget table')
    .should('have.length',2)
    .eq(1).then(tripTableBody => {
      cy.wrap(tripTableBody).within(() => {
        cy.contains('tr',tripTable.tripId).then(tableRow => {

          cy.wrap(tableRow).find('td:nth(1)').should('contain', tripTable.tripDateRequest)
          cy.wrap(tableRow).find('td:nth(3)').should('contain', tripTable.tripId)

          // Change:
      		const tripStatus = _.camelCase(tripTable.status)
          tripTable.status = tripStatus.charAt(0).toUpperCase()+ tripStatus.slice(1)


          cy.wrap(tableRow).find('td:nth(4)').should('contain', tripTable.status)
          cy.wrap(tableRow).find('td:nth(5)').should('contain', tripTable.serviceMode)
          cy.wrap(tableRow).find('td:nth(6)').should('contain', tripTable.legId)
          cy.wrap(tableRow).find('td:nth(7)').should('contain', tripTable.riderId)

          cy.wrap(tableRow).find('td:nth(8)').should('contain', tripTable.firstName)
          cy.wrap(tableRow).find('td:nth(9)').should('contain', tripTable.lastName)

          cy.wrap(tableRow).find('td:nth(10)').should('contain', tripTable.pickAddress)
          cy.wrap(tableRow).find('td:nth(11)').should('contain', tripTable.pickCity)

          cy.wrap(tableRow).find('td:nth(12)').should('contain', tripTable.dropAddress)
          cy.wrap(tableRow).find('td:nth(13)').should('contain', tripTable.dropCity)

        })

      })
    })

}

// Search Booking tables:
// before function change checkFirstBooking

checkBookingTable(bookingTable) {

    const bookingId = bookingTable.bookingId

    // .previous-booking-widget table
    cy.get('.previous-booking-widget table')
    .should('have.length',2)
    .eq(1).then(bookingTableBody => {
      cy.wrap(bookingTableBody).within(() => {
        cy.contains('tr',bookingId).then(tableRow => {

        cy.wrap(tableRow).find('.can-focus').should('be.enabled')
        cy.wrap(tableRow).find('td:nth(0)').click()
        cy.wrap(tableRow).find('td:nth(1)').should('contain.text',bookingTable.tripDateRequest)
        cy.wrap(tableRow).find('td:nth(2)').should('contain.text',bookingTable.bookingId)

        cy.log(bookingTable.status)
        if (bookingTable.status === "willCall") {
          bookingTable.status = 'Will call'
        } else {
          const bookingStatus = _.camelCase(bookingTable.status)
          bookingTable.status = bookingStatus.charAt(0).toUpperCase()+ bookingStatus.slice(1)
        }

        // Make Sure
        cy.wrap(tableRow).find('td:nth(3)').should('contain.text',bookingTable.status)
        cy.wrap(tableRow).find('td:nth(4)').should('contain.text',bookingTable.riderID)
        cy.wrap(tableRow).find('td:nth(5)').should('contain.text',bookingTable.firstName)
        cy.wrap(tableRow).find('td:nth(6)').should('contain.text',bookingTable.lastName)

        cy.wrap(tableRow).find('td:nth(7)').should('contain', bookingTable.pickAddress)
        // 7 W 92ND ST, NEW YORK, NY 10025   -> table
        // 7 W 92ND ST, NEW YORK, NY, 10025  -> display (Extra space)

        cy.wrap(tableRow).find('td:nth(8)').should('contain.text',bookingTable.pickCity)

        cy.wrap(tableRow).find('td:nth(9)').should('contain.text',bookingTable.dropAddress)
        cy.wrap(tableRow).find('td:nth(10)').should('contain.text',bookingTable.dropCity)

        cy.wrap(tableRow).find('td:nth(11)').should('contain.text',bookingTable.groupBooking)

        //cy.wrap(tableRow).find('td').eq(9).should('contain.text',bookingTable.groupBooking)
      })
      })
  })
}


// Search Passenger tables:
checkFirstActiveRider(riderID) {
    cy.get('table').contains('tr',riderID).then(tableRow => {
        cy.wrap(tableRow).find('.can-focus').should('be.enabled')
        cy.wrap(tableRow).find('td').eq(0).click()
    })
}

checkRiderTable(riderObj) {
    cy.get('.lt-body')
      .parents('.passengers-booking-widget')
      .find('tbody tr')
      .find('td')
      .then(tableColumns => {
      cy.wrap(tableColumns).eq(1).should('contain', riderObj.Status)
      cy.wrap(tableColumns).eq(2).should('contain', riderObj.PassengerID)
      cy.wrap(tableColumns).eq(3).should('contain', riderObj.FirstName)
      cy.wrap(tableColumns).eq(4).should('contain', riderObj.LastName)
      cy.wrap(tableColumns).eq(5).should('contain', riderObj.MiddleName)

      cy.wrap(tableColumns).eq(6).should('contain', riderObj.PhoneNumber)
      cy.wrap(tableColumns).eq(7).should('contain', riderObj.DOB)
      cy.wrap(tableColumns).eq(8).should('contain', riderObj.EligibilityStart)
      cy.wrap(tableColumns).eq(9).should('contain', riderObj.EligibilityEnd)
  })
}

checkActiveRiderID(idValue) {
  cy.get('.passengers-booking-widget table')
        .should('have.length',2)
        .eq(1).then(passengerTableBody => {
          cy.wrap(passengerTableBody).within(() => {
            cy.contains('tr',idValue).then(tableRow => {
              cy.wrap(tableRow).find('.can-focus').should('be.enabled').check()
              //cy.wrap(tableRow).find('td:first').check()
              cy.wrap(tableRow).find('td:nth(1)').should('contain.text','Active')
              cy.wrap(tableRow).find('td:nth(2)').should('contain.text',idValue)
            })
          })
        })
}

checkRiderNames(firstName, lastName) {
    cy.get('.passengers-booking-widget table')
    .should('have.length',2)
    .eq(1).then(passengerTableBody => {
        cy.wrap(passengerTableBody).within(() => {
          cy.contains('tr',lastName).then(tableRow => {
            cy.wrap(tableRow).find('.can-focus').should('be.enabled')
            cy.wrap(tableRow).find('td:nth(3)').should('contain.text',firstName)
            cy.wrap(tableRow).find('td:nth(4)').should('contain.text',lastName)
          })

        })
    })
}

}

export const onSearchPassengerSelectionPage = new SearchPassengerSelectionPage()