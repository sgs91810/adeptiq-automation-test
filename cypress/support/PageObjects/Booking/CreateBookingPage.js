/// <reference types="cypress" />

import {
    waitForSpinnerDisapper
} from '../Global/GlobalPage'

export class CreateBookingPage {

    checkCreateBookingHeader() {
        cy.get('.g-side-drawer-header-title').should('have.text','Create Booking')
    }

    // Passenger List
    expandPassengerList() {
        //cy.get('.fa-caret-right').eq(0).click()
        // g-side-drawer-panel-header-caret
        //cy.get('.g-side-drawer-panel-title').contains('Passenger List (1)').click()
        cy.contain('.g-side-drawer-panel-title','Passenger List (1)').click()
    }

    checkPassengerLists() {

        // .passanger-list-form table tr => 6 results
        cy.get('.passanger-list-form')
        .find('table')
        .find('tr')
        .then(tableLists => {

            // Test:
            //cy.wrap(tableLists).first().find('td').eq(0).should('contain','First Name')

            //cy.wrap(tableLists).first().find('td').eq(1).invoke('prop','value')
            //.should('contain','WALDENE')

            //

            cy.wrap(tableLists).eq(4).find('td').eq(1).invoke('prop','value')
            .should('contain','718-4641909')

        })

    }

}

export const onCreateBookingPage = new CreateBookingPage()
