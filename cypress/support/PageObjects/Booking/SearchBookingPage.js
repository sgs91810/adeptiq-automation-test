/// <reference types="cypress" />

import {
    waitForSpinnerDisapper
} from '../Global/GlobalPage'

export class SearchBookingPage {

    clickCreateBookingButton() {
        cy.contains('button','Create Booking').should('be.enabled')
        cy.contains('button','Create Booking').click()
       // waitForSpinnerDisapper()
    }

    clickCreateSubscriptionButton() {
        cy.contains('button','Create Subscription').should('be.enabled')
        cy.contains('button','Create Subscription').click()
        //waitForSpinnerDisapper()
    }

}

export const onSearchBookingPage = new SearchBookingPage()