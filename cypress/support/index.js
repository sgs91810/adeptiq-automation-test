// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
import './Booking/bookingApiCommands'
import './Booking/bookingGuiCommands'
import './Booking/SearchBooking/searchBookingApiCommands'
import './Booking/SearchBooking/searchBookingGuiCommands'
import './Booking/CreateBooking/createBookingApiCommands'
import './Booking/CreateBooking/createBookingGuiCommands'

import './Common/commonApiCommands'
import './Common/commonGuiCommands'

import './Error/errorGuiCommands'
import './FilterSettings/filterSettingsGuiCommands'
import './Geocoding/geocodingApiCommands'
import './Global/spinnerGuiCommands'

import './Schedule/scheduleApiCommands'
import './Schedule/scheduleGuiCommands'

import './SystemConfiguration/systemConfigApiCommands'
import './SystemConfiguration/systemConfigGuiCommands'

import 'cypress-mochawesome-reporter/register';
require('cypress-plugin-tab')
require('cypress-grep')()

// Alternatively you can use CommonJS syntax:
// require('./commands')

// Prevent cypress from failing test
// reference: https://docs.cypress.io/api/events/catalog-of-events#cy
Cypress.on('uncaught:exception', (err,runnable) => {
    return false
})