/// <reference types="cypress" />
import { onSearchPassengerSelectionPage } from "../../../support/PageObjects/Booking/SearchPassengerSelectionPage"
import { onSearchBookingPage } from "../../../support/PageObjects/Booking/SearchBookingPage"
import { onCreateBookingPage } from "../../../support/PageObjects/Booking/CreateBookingPage"
//import rider from "../../../../app/models/rider"
import { dateTimeInfo } from "../../../utils/DateTimeInfo"
const moment = require('moment')

describe('Given at the Create Booking Screen', ()=>{

    beforeEach(()=>{
        cy.loginWithAPI()
        cy.bookingLandingWorkspaceInterceptAPIs()
        cy.visit('/dashboard/Booking/workspaces-booking%2Fbooking')
        cy.checkUrlLocation('/dashboard/Booking/workspaces-booking%2Fbooking')
        cy.bookingLandingWorkspaceWaitAPIs()

        const riderID = '1196350'

        cy.get('@token').then(token => {
            cy.api_checkEligibilityCategory(token,'cONTINUALFULL','system_config/eligibility_category/Continual_Full.json')
        })

        cy.fixture('booking/Multiple_Active_Riders.json').then(multiActiveFixture => {
            cy.getRiderNamesInterceptAPI('true','John','Lee','',multiActiveFixture)
              .as('getMultiActiveResults')
        })

        cy.fixture('booking/Rider_John_Lee_Info.json').then(infoFixture => {
          cy.getRiderIDInterceptAPI(riderID,infoFixture)
            .as('getRiderInfo')
        })

        cy.fixture('booking/Rider_John_Lee_Eligibility.json').then(eligibilityFixture => {
            eligibilityFixture.categories[0].begin = dateTimeInfo.getTodayISOFormat()
            eligibilityFixture.categories[0].end = dateTimeInfo.getAfterTomorrowISOFormat()
            cy.getRiderEligibilityReplyInterceptAPI(eligibilityFixture)
              .as('getEligibility')
        })

          cy.get('input[placeholder="First Name"]').type('John')
          cy.get('input[placeholder="Last Name"]').type('Lee')
          cy.clickSearchRiderButton('@getMultiActiveResults')

          cy.get('.passengers-booking-widget table [data-record-id='+riderID+']')
            .then(tr => {
                cy.wrap(tr).find('input.can-focus').should('be.enabled')
                // Check: not checked before
                cy.wrap(tr).find('div.data-test-table-check-box-cell  input.can-focus').check()
                cy.wrap(tr).find('div.data-test-table-check-box-cell > i').should('have.class', 'is-checked')
            })

        cy.displaySpinnerAPIWait('@getEligibility')

        onSearchBookingPage.clickCreateBookingButton()
        cy.waitForCreateBookingPage()
    })

    context('When I check for rider service need',()=>{
        beforeEach(()=>{
            cy.get('@token').then(token => {
                cy.api_checkserviceNeedsTypes(token,'cURBTOCURB','system_config/service_needs_types/CurbToCurb.json')
            })
        })

        it('Then I should be able to see service needs types if included in the configuration.',()=>{
            cy.get('@token').then(token => {
              cy.api_getServiceNeedsTypes(token,'cURBTOCURB')
                .then(response => {
                    expect(response.status).to.equal(200)
                    const payload = response.body
                    const serviceNeedsValues = JSON.parse(payload.data.attributes.value)
                    const serviceNeedsDisplayName = serviceNeedsValues.displayName

                    cy.fixture('booking/Rider_John_Lee_Info.json').then(infoFixture => {
                        expect(infoFixture.serviceNeeds[0]).to.be.equal(serviceNeedsDisplayName)
                    })

                    cy.contains('h2.g-side-drawer-panel-title','Service Needs').should('be.visible')
                      .click({force: true})
                    cy.get('div.service-needs-form').within(()=>{
                        cy.get('.g-side-drawer-panel-selector .form-control').invoke('prop','value').should('contain',serviceNeedsDisplayName)
                    })
                })
              })
        })
    })

    context('When I try to have same day booking during the disallowed time',()=>{
        beforeEach(()=>{
            cy.get('@token').then(token => {
                cy.api_checkPatchConfigType(token,'config-Book_Trips/cutOffTime4SameDay',
                'system_config/booking_trips/Cutoff_Time_Same_Day.json')
            })
        })

        it('Then I should be able to see same day booking warning dialog modal.',()=>{
              cy.get('@token').then(token => {
              cy.api_getConfiguration(token,'config-Book_Trips/cutOffTime4SameDay')
                .then(response => {
                    expect(response.status).to.equal(200)
                    const payload = response.body
                    const sameDayBookingCutoffMin = payload.data.attributes.value
                    cy.contains('h2.g-side-drawer-panel-title','Trip Details').should('be.visible')

                    const sameDayTimeStamp = moment().add(sameDayBookingCutoffMin,'minutes').format('X')
                    const endDayTimeStamp = moment().endOf('day').format('X')

                    let sameDayBookingNotAllowTime
                    if (sameDayTimeStamp <= endDayTimeStamp) {
                        sameDayBookingNotAllowTime = moment().add(sameDayBookingCutoffMin,'minutes').subtract(1,'minutes').format('h:mm A')
                    } else {
                        sameDayBookingNotAllowTime =  moment().endOf('day').format('h:mm A')
                    }

                    cy.get('[placeholder="hh:mm A"]').clear().type(sameDayBookingNotAllowTime,'{enter}')
                    cy.get('.g-side-drawer-panel-form .ember-flatpickr-input:nth(1)').click({force: true})

                    cy.get('div.dayContainer').within(()=>{
                        cy.get('.today').click({force: true})
                    })

                    const today = moment().format('ddd')+', '+moment().format('MMM')+' '+moment().format('D')

                    cy.get('.g-side-drawer-panel-form .ember-flatpickr-input:nth(1)').invoke('prop','value').should('contain',today)

                    cy.contains('.secondary-window-header','Create Booking').should('be.visible')
                    cy.get('.secondary-window-body .dashboard-save-as-body h6').then(warning => {
                        expect(warning.text().trim()).to.be.eq('Same day booking must be at least 60 mins in advance. Please select a different request time.')
                    })
                    cy.contains('.btn-pri','OK').click({force: true})
                })
              })
        })
    })

    context('When I click on Passenger List label',()=>{
        beforeEach(()=>{

        })

        it('Then I see Header Title and Passenger Full Name.',()=>{
            cy.contains('h1.g-side-drawer-header-title','Create Booking').should('be.visible')
            cy.fixture('booking/Rider_John_Lee_Info.json').then(infoFixture => {
                const fullName = infoFixture.firstName.concat(' ',infoFixture.middleName,' ',infoFixture.lastName)
                cy.get('.passenger-name-list').invoke('text').then((text)=>{
                    expect(text.trim()).equal(fullName)
                })
            })
        })

        it('Then I see Passenger Profile.',()=>{
            cy.contains('.g-side-drawer-panel-title','Passenger List (1)').then(passengerList =>{
                cy.wrap(passengerList).should('be.visible')
                cy.wrap(passengerList).prev().should('have.class','g-side-drawer-panel-header-caret')
                             .children('.fa-caret-right').should('be.visible')

                cy.wrap(passengerList).click()
                cy.wrap(passengerList).prev().should('have.class','g-side-drawer-panel-header-caret')
                             .children('.fa-caret-down').should('be.visible')
            })

            cy.get('div.passanger-list-form table.g-side-drawer-panel-form tr').should('have.length',6)
              .each((passList, index) => {
                cy.fixture('booking/Rider_John_Lee_Info.json').then(infoFixture => {
                    const mainNumber = infoFixture.notificationPhoneNumbers.find((num) => {
                        return num.type === 'MAIN'
                    })

                    let pNumber = mainNumber.phoneNumber
                    pNumber = pNumber.substring(0,3)+"-"+pNumber.substring(3,pNumber.length);
                    const phoneNumber = mainNumber.areaCode+'-'+pNumber

                    const passListInfo = {
                        'First Name': infoFixture.firstName,
                        'Last Name': infoFixture.lastName,
                        'ID': infoFixture.id,
                        'Passcode': infoFixture.passcode,
                        'Phone Number': phoneNumber,
                        'Notes': infoFixture.notes
                    }

                    if (index===0) {
                       cy.wrap(passList).find('td:first').should('have.text',Object.keys(passListInfo)[index])
                    } else {
                       cy.wrap(passList).find('td:first').invoke('text').then((label)=> {
                            expect(label.trim()).equal(Object.keys(passListInfo)[index])
                       })
                    }

                    let tagName=''
                    if (index <5) {
                        tagName = 'input'
                    } else {
                        tagName = 'textarea'
                    }

                    cy.wrap(passList).find('td:nth(1)').find(tagName)
                     .invoke('prop','value').should('contain',Object.values(passListInfo)[index])
                })
              })

        })
    })
})