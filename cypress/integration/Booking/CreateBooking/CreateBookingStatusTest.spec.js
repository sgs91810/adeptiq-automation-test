/// <reference types="cypress" />
import { onSearchPassengerSelectionPage } from "../../../support/PageObjects/Booking/SearchPassengerSelectionPage"
import { onSearchBookingPage } from "../../../support/PageObjects/Booking/SearchBookingPage"
import { onCreateBookingPage } from "../../../support/PageObjects/Booking/CreateBookingPage"

import { bookingCreationInfo} from "../../../utils/BookingCreationInfo"
import { dateTimeInfo } from "../../../utils/DateTimeInfo"
import { stringInfo } from "../../../utils/StringInfo"
import { tripInfo } from "../../../utils/TripInfo"

const moment = require('moment')
const { _ } = Cypress

describe('Given at the Booking Workspace', ()=> {
    beforeEach(()=>{
        cy.loginWithAPI()
        cy.bookingLandingWorkspaceInterceptAPIs()
        cy.visit('/dashboard/Booking/workspaces-booking%2Fbooking')
        cy.checkUrlLocation('/dashboard/Booking/workspaces-booking%2Fbooking')
        cy.bookingLandingWorkspaceWaitAPIs()
    })

    context('When I book for taxi mode booking trip for tomorrow through API',()=> {
        beforeEach(()=>{
            cy.get('@token').then(token => {
                cy.api_checkEligibilityCategory(token,'cONTINUALFULL','system_config/eligibility_category/Continual_Full.json')
                .then(response => {
                    expect(response.status).to.equal(200)
                })
            })
            cy.checkFilterSettings('[data-filter-class="booking"]')
            cy.checkFilterSettings('[data-filter-class="trip"]')
        })

    it('Then I should be able to see scheduled taxi in the booking & trip table.',function(){
        const bookingTable = {},
        tripTable = {},
        geoNodes = {},
        tomorrow = dateTimeInfo.getDateTime(9,30,86400000),
        primaryDateTomorrow = moment(tomorrow).format('YYYY-MM-DD'),
        bookingDateTomorrow =primaryDateTomorrow+'B',
        tomorrowDateFormat = moment(tomorrow).format('YYYY-MM-DD'),
        tomorrowTimeFormat = moment(tomorrow).format('hh:mm A')

        cy.externalRiderInterceptAPI('booking/Rider_75_Info.json')
        cy.selectRiderInterceptBookingTable()

        let bookingId,
        bookingScheduleAsynOpId,  // Booking Schedule Info
        deleteScheduleAsynOpId,   // Delete Schedule Info
        primaryScheduleAsynOpId,  // Primary Schedule Info
        bookingSchedulerId,       // Temporary fix:
        primarySchedulerId

        // 1. Booking with promise time
        cy.get('@token').then(token =>{
            cy.fixture('booking/Booking_PromiseTime_75.json').then(bookingFixture => {
                const bookingArray = bookingFixture.included
                const segmentStopArray = bookingArray.filter( ({type}) => type ==='segmentStop' );

                const pickAddressInfo = bookingCreationInfo.getBookingAddressInfo(bookingArray,segmentStopArray,'pick')
                const pickIndex = pickAddressInfo.stopIndex
                const pickAddress = pickAddressInfo.freeformAddress
                tripTable.pickAddress = bookingTable.pickAddress = pickAddressInfo.address
                tripTable.pickCity = bookingTable.pickCity = pickAddressInfo.city

                const dropAddressInfo = bookingCreationInfo.getBookingAddressInfo(bookingArray,segmentStopArray,'drop')
                const dropIndex = dropAddressInfo.stopIndex
                const dropAddress = dropAddressInfo.freeformAddress
                tripTable.dropAddress = bookingTable.dropAddress = dropAddressInfo.address
                tripTable.dropCity = bookingTable.dropCity = dropAddressInfo.city

                const segment = bookingArray.find( ({type}) => type ==='segment' );
                const anchor = segment.attributes.anchor
                segment.attributes.promiseTime = tomorrow.toISOString()

                tripTable.tripDateRequest = bookingTable.tripDateRequest = tomorrowDateFormat.concat(" / ",anchor," / ",tomorrowTimeFormat)

                const leg = bookingArray.find( ({type}) => type ==='leg');
                leg.attributes.requestTime = tomorrow.toISOString()

                tripTable.riderId = bookingTable.riderID = leg.relationships.rider.data.riderId
                tripTable.firstName = bookingTable.firstName = leg.relationships.rider.data.firstName
                tripTable.lastName = bookingTable.lastName =  leg.relationships.rider.data.lastName

                bookingTable.groupBooking = "No"

                cy.api_Geocode(token,pickAddress)
                cy.then(()=>{
                    expect(this.getGeocode.status).to.equal(200)
                    segmentStopArray[pickIndex].relationships.location.data.geoNode = this.getGeocode.body.data[0].geonode
                    cy.api_Geocode(token,dropAddress)
                })

                cy.then(()=>{
                    expect(this.getGeocode.status).to.equal(200)
                    segmentStopArray[dropIndex].relationships.location.data.geoNode = this.getGeocode.body.data[0].geonode
                    cy.api_BookingWithPromiseTime(token,bookingFixture)
                })

                cy.then(()=>{
                    expect(this.bookingSchedule.status).to.equal(201)

                    bookingId = this.bookingSchedule.body.bookingId
                    bookingTable.bookingId = bookingId

                    bookingScheduleAsynOpId = this.bookingSchedule.body.promiseTimeAsyncOperationInfoArray[0].data.id
                    deleteScheduleAsynOpId = Number(bookingScheduleAsynOpId) +1

                    cy.api_bookingId(token, bookingId)
                })

                cy.then(()=>{
                    expect(this.bookingID.status).to.equal(200)
                    const bookingArray = this.bookingID.body.included

                    const bookingSegment = bookingArray.find( ({type}) => type ==='segment' );
                    const bookingLeg = bookingArray.find( ({type}) => type ==='leg' );
                    tripTable.legId = bookingLeg.id

                    cy.api_schedule(token,bookingDateTomorrow)
                })

                cy.then(()=>{
                    expect(this.schedule.status).to.equal(200)
                    bookingSchedulerId = this.schedule.body.data[0].id
                    cy.api_schedule(token,primaryDateTomorrow)
                })

                cy.then(()=> {
                    expect(this.schedule.status).to.equal(200)
                    primarySchedulerId = this.schedule.body.data[0].id
                    cy.api_scheduleAynscOperation(token,bookingScheduleAsynOpId)
                })

                cy.then(()=>{
                    expect(this.scheduleAsyncOperation.status).to.equal(200)
                    const tripStatus = tripInfo.tripReqWait(token, bookingId, bookingSchedulerId)

                    if (tripStatus ==false) {
                        cy.contains("Sorry, something went scheduler!").should('not.exist')
                    }
                    cy.api_deleteBooking(token,bookingId)
                })

                cy.then(()=> {
                    //expect(this.deleteBookingTrip.status).to.equal(204) // 202
                    const deleteBody = this.deleteBookingTrip.hasOwnProperty('body')
                    cy.log('deleteBody:'+deleteBody) // Output: true
                    cy.api_scheduleAynscOperation(token,deleteScheduleAsynOpId)
                })

                cy.then(()=>{
                    expect(this.scheduleAsyncOperation.status).to.equal(200)

                    cy.fixture('booking/Primary_Schedule_Booking75.json').then(primaryFixture =>{
                        primaryFixture.data.attributes.temporaryBookingId = bookingId
                        bookingTable.status = primaryFixture.data.attributes.status
                        const primaryArray = primaryFixture.included

                        // mistake: create requested status data - ok for taxi
                        const segment = primaryArray.find( ({type}) => type ==='segment' );
                        segment.attributes.promiseTime = tomorrow.toISOString()
                        tripTable.serviceMode = segment.attributes.travelMode

                        const leg = primaryArray.find( ({type}) => type ==='leg');
                        leg.attributes.requestTime = tomorrow.toISOString()

                        cy.api_primaryBooking(token,primaryFixture)
                    })
                })

                cy.then(()=> {
                    expect(this.primarySchedule.status).to.equal(201)
                    const promiseTimeOperationInfoArray = this.primarySchedule.body.promiseTimeAsyncOperationInfoArray

                    if (promiseTimeOperationInfoArray.length==0) {
                        cy.log('Empty Array')
                        //primaryScheduleAsynOpId = Number(deleteScheduleAsynOpId) +1
                    } else {
                        cy.log('Non-Empty Array')
                        //primaryScheduleAsynOpId = this.primarySchedule.body.promiseTimeAsyncOperationInfoArray[0].data.id
                    }

                    primaryScheduleAsynOpId = Number(deleteScheduleAsynOpId) +1
                    tripInfo.tripReqWait(token, bookingId, primarySchedulerId)

                    cy.api_scheduleTrip(token,bookingId,primarySchedulerId)
                })

                cy.then(()=> {
                    expect(this.scheduleTrip.status).to.equal(200)
                    tripTable.tripId = this.scheduleTrip.body.data[0].attributes.tripId
                    tripTable.status = bookingTable.status // for taxi

        // Front End Checking Code:
        cy.intercept('GET','**/booking/booking?**').as('getBooking')
        cy.intercept('GET','**/scheduling/trip?**').as('getSchedulingTrip')

        const passcodeRiderID = '75'
        onSearchPassengerSelectionPage.enterRiderID(passcodeRiderID)

         cy.clickSearchRiderButton('@getDelayBookingRider')

        onSearchPassengerSelectionPage.checkFirstActiveRider(passcodeRiderID)

        cy.get('@spinner')
        .should('be.visible')

        cy.wait('@getRiderManagment')
        cy.wait('@getBooking')
        cy.wait('@getSchedulingTrip')

        cy.get('@spinner')
          .should('not.exist')
            onSearchPassengerSelectionPage.checkBookingTable(bookingTable)
            onSearchPassengerSelectionPage.checkFirstTripTable(tripTable)
                })

            })
        })
    })
    })

    context('When I save booking trip for tomorrow without scheduling through API',()=>{
        beforeEach(()=>{
            cy.get('@token').then(token => {
                cy.api_checkEligibilityCategory(token,'full','system_config/eligibility_category/Full.json')
                  .then(response => {
                     expect(response.status).to.equal(200)
                  })
            })
            cy.checkFilterSettings('[data-filter-class="booking"]')
        })

    it('Then I should be able to see Will call booking status.', function(){

      cy.externalRiderInterceptAPI('booking/Rider_830666_Info.json')
      cy.selectRiderInterceptBookingTable()

        const bookingTable = {}

        const tomorrow930 = dateTimeInfo.getDateTime(9,30,86400000);
        const tomorrowDateFormat = moment(tomorrow930).format('YYYY-MM-DD')
        const tomorrowTimeFormat = moment(tomorrow930).format('hh:mm A')

        cy.fixture('booking/WillCall_Booking.json').then(willFixture =>{
            willFixture.included[2].attributes.promiseTime = tomorrow930.toISOString()
            willFixture.included[4].attributes.requestTime = tomorrow930.toISOString()

            const bookingArray = willFixture.included
            const segmentStopArray = bookingArray.filter( ({type}) => type ==='segmentStop' );

            const pickAddressInfo = bookingCreationInfo.getBookingAddressInfo(bookingArray,segmentStopArray,'pick')
            bookingTable.pickAddress = pickAddressInfo.address
            bookingTable.pickCity = pickAddressInfo.city

            const dropAddressInfo = bookingCreationInfo.getBookingAddressInfo(bookingArray,segmentStopArray,'drop')
            bookingTable.dropAddress = dropAddressInfo.address
            bookingTable.dropCity = dropAddressInfo.city

            bookingTable.groupBooking = "No"

            const segment = bookingArray.find( ({type}) => type ==='segment' );
            const anchor = segment.attributes.anchor

            const leg = bookingArray.find( ({type}) => type ==='leg');

            bookingTable.tripDateRequest = tomorrowDateFormat.concat(" / ",anchor," / ",tomorrowTimeFormat)
            bookingTable.riderID = leg.relationships.rider.data.riderId
            bookingTable.firstName = leg.relationships.rider.data.firstName
            bookingTable.lastName = leg.relationships.rider.data.lastName

            cy.get('@token').then(token => {
                cy.api_createBookingEntity(token,willFixture)

                cy.then(()=> {         // 2. Check: status: continue.....
                    bookingTable.bookingId = this.noScheduleBooking.body.data.id
                    bookingTable.status = this.noScheduleBooking.body.data.attributes.status

                    const bookingId = bookingTable.bookingId

                    const passcodeRiderID = '830666'
                    onSearchPassengerSelectionPage.enterRiderID(passcodeRiderID)

                   cy.clickSearchRiderButton('@getDelayBookingRider')

                    onSearchPassengerSelectionPage.checkFirstActiveRider(passcodeRiderID)
                    cy.selectRiderWaitBookingTable()

                    // Web Table
                    onSearchPassengerSelectionPage.checkBookingTable(bookingTable)
                   cy.api_deleteBookingOnly(token,bookingId)

                 })

                cy.then(() => {
                   expect(this.deleteBookingOnly.status).to.equal(204)
                })
            })
        })

        // 4. Get: Check get to ensure deletion: Postman to check empty body
        // booking

    })
    })
})