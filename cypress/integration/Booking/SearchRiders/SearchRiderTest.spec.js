/// <reference types="cypress" />
import { onSearchPassengerSelectionPage } from "../../../support/PageObjects/Booking/SearchPassengerSelectionPage"
import { dateTimeInfo } from "../../../utils/DateTimeInfo"
import { stringInfo } from "../../../utils/StringInfo"
const { _ } = Cypress
const moment = require('moment')

describe('Given I am at the Booking Workspace page',() => {
    before(()=>{

    })

    beforeEach(() => {
      cy.loginWithAPI()
      cy.bookingLandingWorkspaceInterceptAPIs()
      cy.visit('/dashboard/Booking/workspaces-booking%2Fbooking')
      cy.checkUrlLocation('/dashboard/Booking/workspaces-booking%2Fbooking')
      cy.bookingLandingWorkspaceWaitAPIs()
    })

    context('When I search for a rider by ID',() => {
      beforeEach(()=>{
        cy.get('@token').then(token => {
          cy.api_checkEligibilityCategory(token,'full','system_config/eligibility_category/Full.json')
        })
        cy.externalRiderInterceptAPI('booking/Rider_830666_Info.json')
      })

      it('Then correct passenger info in the passenger table (smoke)', { tags: '@smokeTag' },()=>{
        const riderProfile = {}
        const searchID = 830666

        cy.get('@token').then(token => {
          cy.api_searchRider(token,searchID)
            .then(response => {
                expect(response.status).to.equal(200)
                const payload = response.body

                const riderStatus = _.camelCase(payload.status)
                riderProfile.Status = riderStatus.charAt(0).toUpperCase() + riderStatus.slice(1)

                riderProfile.PassengerID = payload.id
                riderProfile.FirstName = payload.firstName
                riderProfile.LastName = payload.lastName
                riderProfile.MiddleName = payload.middleName

                riderProfile.PhoneNumber = payload.notificationPhoneNumbers[0].areaCode
                +'-'+payload.notificationPhoneNumbers[0].phoneNumber

                riderProfile.DOB = dateTimeInfo.formatDateTwoMonthDayFourYear(payload.dateOfBirth)
                riderProfile.EligibilityStart = dateTimeInfo.formatDateTwoMonthDayFourYear(payload.categories[0].begin)
                riderProfile.EligibilityEnd = dateTimeInfo.formatDateTwoMonthDayFourYear(payload.categories[0].end)
            })
        })
        onSearchPassengerSelectionPage.enterRiderID(searchID)
        cy.clickSearchRiderButton('@getDelayBookingRider')
        onSearchPassengerSelectionPage.checkRiderTable(riderProfile)
     })

    it('Then names and phone number fields to be disabled.', () => {
        const searchID = 830666
        onSearchPassengerSelectionPage.enabledActiveCheckbox()
        onSearchPassengerSelectionPage.enterRiderID(searchID)

        onSearchPassengerSelectionPage.disabledNamesPhoneFields()

        cy.clickSearchRiderButton('@getDelayBookingRider')
        onSearchPassengerSelectionPage.checkActiveRiderID(searchID)
    })

    it('Then clear all search info by clicking the Clear button and ensure outer area in green color.',()=>{
      const searchID = 830666

      cy.get('input[placeholder="ID Number"]').focus()
      onSearchPassengerSelectionPage.checkInputGreenBorderColor()
      cy.tab().focused().should('have.attr', 'name', 'first-name')
      onSearchPassengerSelectionPage.checkInputGreenBorderColor()

      cy.tab().focused().should('have.attr', 'name', 'last-name')
      onSearchPassengerSelectionPage.checkInputGreenBorderColor()

      cy.tab().focused().should('have.attr', 'name', 'phone-number')
      onSearchPassengerSelectionPage.checkInputGreenBorderColor()

      onSearchPassengerSelectionPage.enterRiderID(searchID)
      cy.focused().should('have.attr', 'name', 'passenger-id')
        .tab().focused().should('have.attr', 'type', 'checkbox')
        //onSearchPassengerSelectionPage.checkInputGreenBorderColor()

      cy.tab().focused().should('have.prop', 'tagName').should('eq','BUTTON')
      onSearchPassengerSelectionPage.checkButtonGreenBorderColor()

      cy.tab().focused().should('have.prop', 'tagName').should('eq','BUTTON')
      onSearchPassengerSelectionPage.checkButtonGreenBorderColor()

      cy.clickSearchRiderButton('@getDelayBookingRider')

      cy.contains('.passengers-booking-widget table tr',searchID).then(tableRow => {
        cy.wrap(tableRow).should('have.length',1)
      })

      onSearchPassengerSelectionPage.clickClearButton()
      cy.get('.passengers-booking-widget table tr.lt-no-data').should('have.length',1)

      cy.get('input[placeholder="ID Number"]').should('be.empty')
      cy.get('input[placeholder="First Name"]').should('be.empty')
      cy.get('input[placeholder="Last Name"]').should('be.empty')
      cy.get('input[placeholder="000 000 0000"]').should('be.empty')
    })

  })

  context('When I search for correct passenger names',()=>{
    const fName = 'EVANGELINE'
    const lName = 'OFORI ATTA'
    let phoneNumber = '6465902499'

    beforeEach(()=>{
      cy.get('@token').then(token => {
        cy.api_checkEligibilityCategory(token,'full','system_config/eligibility_category/Full.json')
      })

      cy.fixture('booking/searchRider/Rider_830666_Result.json').then(riderFixture => {
        cy.getRiderNamesInterceptAPI('true','Tom','Lee','',riderFixture)
          .as('getDelayBookingRider')
      })
    })

    it('Then disabled ID textbox field.', () => {
        onSearchPassengerSelectionPage.enterNamesWithManualEnterKey(fName, lName)
        cy.displaySpinnerNoAPIWait()
        onSearchPassengerSelectionPage.checkRiderNames(fName, lName)
    })

    it('Then correct search results along with main phone number and ensure Green Active Only Checkbox input borders.', ()=>{
      const phoneNumberDisplay = '646-5902499'

      onSearchPassengerSelectionPage.enterFirstName(fName)
      onSearchPassengerSelectionPage.enterLastName(lName)
      onSearchPassengerSelectionPage.enterPhoneNumber(phoneNumber)
      onSearchPassengerSelectionPage.enabledActiveCheckbox()
      onSearchPassengerSelectionPage.clickSearchButton()

      cy.displaySpinnerNoAPIWait()

      cy.get('.passengers-booking-widget table')
        .should('have.length',2)
        .eq(1).then(passengerTableBody => {
          cy.wrap(passengerTableBody).within(() => {
            cy.contains('tr',fName).then(tableRow => {
              cy.wrap(tableRow).find('.can-focus').should('be.enabled')
              cy.wrap(tableRow).find('td:nth(3)').should('contain.text',fName)
              cy.wrap(tableRow).find('td:nth(4)').should('contain.text',lName)
              cy.wrap(tableRow).find('td:nth(6)').should('contain.text',phoneNumberDisplay)
            })
          })
      })

    })

    it('Then correct search results along with other phone number.', ()=>{
      const otherNumber = '9292862036'
      onSearchPassengerSelectionPage.enterLastName(lName)
      onSearchPassengerSelectionPage.enterPhoneNumber(otherNumber)
      onSearchPassengerSelectionPage.clickSearchButton()
      cy.displaySpinnerNoAPIWait()
      onSearchPassengerSelectionPage.checkRiderNames(fName, lName)
    })

    it('Then no search result with incomplete phone number.', ()=>{
      phoneNumber = '718271217' // incomplete number
      const alertText = 'A full 10-digit phone number is required in order to search.'
      onSearchPassengerSelectionPage.enterLastName(lName)
      onSearchPassengerSelectionPage.enterPhoneNumber(phoneNumber)
      onSearchPassengerSelectionPage.clickSearchButton()
      cy.getErrorNotification(alertText)
    })
  })

  context('When I search for Inactive Riders ',()=>{
      beforeEach(()=>{
        cy.fixture('booking/Rider_830666_Info.json').then(riderFixture => {
          riderFixture.status="INACTIVE"
          riderFixture.categories[0].type=""
          cy.riderReplyInterceptAPI(riderFixture)
        })
      })

      it('Then disenabled status checkbox and Create & Subscription buttons.',()=>{
        const searchID = 830666

        onSearchPassengerSelectionPage.enterRiderID(searchID)
        onSearchPassengerSelectionPage.uncheckActiveOnly()
        cy.clickSearchRiderButton('@getDelayBookingRider')

        cy.get('table').contains('tr',searchID).then(tableRow => {
            cy.wrap(tableRow).find('.can-focus').should('be.disabled')
            cy.wrap(tableRow).find('td').eq(1).should('contain.text','Inactive')
            cy.wrap(tableRow).find('td').eq(2).should('contain.text',searchID)
        })
        cy.contains('button','Create Subscription').should('be.disabled')
        cy.contains('button','Create Subscription').should('be.disabled')

      })
    })

    context('When I search for Pending status Riders',()=>{
      it('Then should be able to create booking with PRESUMPTIVE COND type.',()=>{
        cy.get('@token').then(token => {
          cy.api_checkEligibilityCategory(token,'pRESUMPTIVECOND','system_config/eligibility_category/Presumptive_Cond.json')
        })

        cy.fixture('booking/Rider_830666_Info.json').then(riderFixture => {
          riderFixture.status="PENDING"
          riderFixture.categories[0].type="PRESUMPTIVE COND"
          cy.riderReplyInterceptAPI(riderFixture)
        })

        cy.fixture('booking/Rider_830666_Eligibility.json').then(riderEligibilityFixture => {
          riderEligibilityFixture.status="PENDING"
          riderEligibilityFixture.categories[0].type="PRESUMPTIVE COND"
          riderEligibilityFixture.categories[0].begin = dateTimeInfo.getTodayISOFormat()
          riderEligibilityFixture.categories[0].end = dateTimeInfo.getAfterTomorrowISOFormat()
          cy.getRiderEligibilityReplyInterceptAPI(riderEligibilityFixture)
        })

        const searchID = 830666
        onSearchPassengerSelectionPage.enterRiderID(searchID)
        cy.clickSearchRiderButton('@getDelayBookingRider')
        onSearchPassengerSelectionPage.checkActiveRiderID(searchID)
        cy.contains('button','Create Booking').should('be.enabled')
        cy.contains('button','Create Subscription').should('be.enabled')
      })

      it('Then should be able to create booking with PRESUMPTIVE ELIG type.',()=>{
        cy.get('@token').then(token => {
          cy.api_checkEligibilityCategory(token,'pRESUMPTIVEELIG','system_config/eligibility_category/Presumptive_Elig.json')
        })

        cy.fixture('booking/Rider_830666_Info.json').then(riderFixture => {
          riderFixture.status="PENDING"
          riderFixture.categories[0].type="PRESUMPTIVE ELIG"
          cy.riderReplyInterceptAPI(riderFixture)
        })

        cy.fixture('booking/Rider_830666_Eligibility.json').then(riderEligibilityFixture => {
          riderEligibilityFixture.status="PENDING"
          riderEligibilityFixture.categories[0].type="PRESUMPTIVE ELIG"
          riderEligibilityFixture.categories[0].begin = dateTimeInfo.getTodayISOFormat()
          riderEligibilityFixture.categories[0].end = dateTimeInfo.getAfterTomorrowISOFormat()
          cy.getRiderEligibilityReplyInterceptAPI(riderEligibilityFixture)
        })

        const searchID = 830666
        onSearchPassengerSelectionPage.enterRiderID(searchID)
        cy.clickSearchRiderButton('@getDelayBookingRider')
        onSearchPassengerSelectionPage.checkActiveRiderID(searchID)
        cy.contains('button','Create Booking').should('be.enabled')
        cy.contains('button','Create Subscription').should('be.enabled')
      })
    })

    context('When error occur',()=>{
      it('Then correct server error message upon server error.',()=>{
        const searchID = 40
        const errorMsg = 'Search error. Try again. If the issue persists, please contact your system administrator.'

        cy.intercept('GET','**/booking/rider-external/*',{statusCode:500}).as('serverFailure')

        onSearchPassengerSelectionPage.enabledActiveCheckbox()
        onSearchPassengerSelectionPage.enterRiderID(searchID)
        cy.contains('button','Search').should('not.be.disabled')
        cy.contains('button','Search').click()

        cy.wait('@serverFailure')
        cy.get('@serverFailure').then(xhr => {
            cy.log(xhr)
            expect(xhr.response.statusCode).to.equal(500)
            cy.get('.alert-text').contains(errorMsg)
        })
    })

    it('Then correct server error message upon network failure error.',()=>{
        const searchID = 40
        const errorMsg = 'Search error. Try again. If the issue persists, please contact your system administrator.'

        cy.intercept('GET','**/booking/rider-external/*',{forceNetworkError:true}).as('networkFailure')

        onSearchPassengerSelectionPage.enabledActiveCheckbox()
        onSearchPassengerSelectionPage.enterRiderID(searchID)
        cy.contains('button','Search').should('not.be.disabled')
        cy.contains('button','Search').click()

        cy.wait('@networkFailure')
        cy.get('@networkFailure').then(xhr => {
            cy.log(xhr)
            cy.get('.alert-text').contains(errorMsg)
        })
      })
   })

   context('When I search for more than 300 search results ',()=>{
    it('Then I should be able to see correct warning message.', () => {
      const warningMsg = 'Too many matching results were found. Displaying the first 300 results. Please refine the search parameters'
      const searchFirstName = 'Ch'

      onSearchPassengerSelectionPage.enterFirstName(searchFirstName)
      onSearchPassengerSelectionPage.uncheckActiveOnly()
      onSearchPassengerSelectionPage.disabledIdField()
      onSearchPassengerSelectionPage.clickSearchButton()
      cy.displaySpinnerNoAPIWait()

      onSearchPassengerSelectionPage.getAlertMessage(warningMsg)
      cy.get('.notification-bar .alert-warning').should('have.css', 'background-color', 'rgb(227, 97, 25)')
    })
   })

   context('When I click on Passenger Table Header Column',()=>{
    const sortingSearchResults = require('../../../fixtures/booking/Sorting_Search_Results')
    const passengerColumns = {}
    beforeEach(()=>{
      cy.get('@token').then(token => {
        cy.api_checkEligibilityCategory(token,'tEMPORARYFULL','system_config/eligibility_category/Temporary_Full.json')
      })
      cy.fixture('booking/Sorting_Search_Results.json').then(sortingSearchFixture => {
        cy.getRiderNamesInterceptAPI('false','Tom','Lee','',sortingSearchFixture)
          .as('getSortingSearchResults')
      })
      cy.get('input[placeholder="First Name"]').type('Tom')
      cy.get('input[placeholder="Last Name"]').type('Lee')
      onSearchPassengerSelectionPage.uncheckActiveOnly()
      cy.clickSearchRiderButton('@getSortingSearchResults')
    })

    // Status:
    it('Then Passenger Data should be sorted by Status column in ascending and descending order.',()=>{
      passengerColumns.ascSortLocator = '.column-controls:first .fa-sort-asc'
      passengerColumns.descSortLocator = '.column-controls:first .fa-sort-desc'
      passengerColumns.columnLabelText = 'Status'

      passengerColumns.firstRowActualDataLocator = 'td.passengerStatus > div:first'
      passengerColumns.lastRowActualDataLocator = 'td.passengerStatus > div:last'

      passengerColumns.firstAscRowExpectedData = stringInfo.capitalizeFirstLetter(sortingSearchResults.riders[2].status)
      passengerColumns.lastAscRowExpectedData =  stringInfo.capitalizeFirstLetter(sortingSearchResults.riders[0].status)
      passengerColumns.firstDescRowExpectedData = stringInfo.capitalizeFirstLetter(sortingSearchResults.riders[1].status)
      passengerColumns.lastDescRowExpectedData =  stringInfo.capitalizeFirstLetter(sortingSearchResults.riders[2].status)

      onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
    })

    // Passenger ID:
    it('Then Passenger Data should be sorted by Passenger ID column in ascending and descending order.',()=>{
      passengerColumns.ascSortLocator =  '.column-controls:nth(1) .fa-sort-asc'
      passengerColumns.descSortLocator = '.column-controls:nth(1) .fa-sort-desc'
      passengerColumns.columnLabelText = 'Passenger ID'

      passengerColumns.firstRowActualDataLocator = 'td.passengerId > div:first'
      passengerColumns.lastRowActualDataLocator =  'td.passengerId > div:last'

      passengerColumns.firstAscRowExpectedData = sortingSearchResults.riders[0].id
      passengerColumns.lastAscRowExpectedData =  sortingSearchResults.riders[2].id
      passengerColumns.firstDescRowExpectedData = sortingSearchResults.riders[2].id
      passengerColumns.lastDescRowExpectedData =  sortingSearchResults.riders[0].id

      onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
    })

    // First Name:
    it('Then Passenger Data should be sorted by First Name column in ascending and descending order.',()=>{
      passengerColumns.ascSortLocator =  '.column-controls:nth(2) .fa-sort-asc'
      passengerColumns.descSortLocator = '.column-controls:nth(2) .fa-sort-desc'
      passengerColumns.columnLabelText = 'First Name'

      passengerColumns.firstRowActualDataLocator = 'td.passengerFirstName > div:first'
      passengerColumns.lastRowActualDataLocator =  'td.passengerFirstName > div:last'

      passengerColumns.firstAscRowExpectedData = sortingSearchResults.riders[1].firstName
      passengerColumns.lastAscRowExpectedData =  sortingSearchResults.riders[2].firstName
      passengerColumns.firstDescRowExpectedData = sortingSearchResults.riders[2].firstName
      passengerColumns.lastDescRowExpectedData =  sortingSearchResults.riders[0].firstName

      onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
    })

    // Last Name:
    it('Then Passenger Data should be sorted by Last Name column in ascending and descending order.',()=>{
      passengerColumns.ascSortLocator =  '.column-controls:nth(3) .fa-sort-asc'
      passengerColumns.descSortLocator = '.column-controls:nth(3) .fa-sort-desc'
      passengerColumns.columnLabelText = 'Last Name'

      passengerColumns.firstRowActualDataLocator = 'td.passengerLastName > div:first'
      passengerColumns.lastRowActualDataLocator =  'td.passengerLastName > div:last'

      passengerColumns.firstAscRowExpectedData = sortingSearchResults.riders[0].lastName
      passengerColumns.lastAscRowExpectedData =  sortingSearchResults.riders[1].lastName
      passengerColumns.firstDescRowExpectedData = sortingSearchResults.riders[1].lastName
      passengerColumns.lastDescRowExpectedData =  sortingSearchResults.riders[2].lastName

      onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
    })

    // Middle Name:
    it('Then Passenger Data should be sorted by Middle Name column in ascending and descending order.',()=>{
        passengerColumns.ascSortLocator =  '.column-controls:nth(4) .fa-sort-asc'
        passengerColumns.descSortLocator = '.column-controls:nth(4) .fa-sort-desc'
        passengerColumns.columnLabelText = 'Middle Name'

        passengerColumns.firstRowActualDataLocator = 'td.passengerMiddleName  > div:first'
        passengerColumns.lastRowActualDataLocator =  'td.passengerMiddleName  > div:last'

        passengerColumns.firstAscRowExpectedData = sortingSearchResults.riders[0].middleName
        passengerColumns.lastAscRowExpectedData = sortingSearchResults.riders[2].middleName
        passengerColumns.firstDescRowExpectedData = sortingSearchResults.riders[2].middleName
        passengerColumns.lastDescRowExpectedData =  sortingSearchResults.riders[0].middleName

        onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
      })

    // Phone Number:
    it('Then Passenger Data should be sorted by Phone Number column in ascending and descending order.]',()=>{
      const areaCode = sortingSearchResults.riders[0].notificationPhoneNumbers[0].areaCode
      const otherNumber = sortingSearchResults.riders[0].notificationPhoneNumbers[0].phoneNumber
      const phoneNum = areaCode.concat('-',otherNumber)
      const firstPhoneNumber = sortingSearchResults.riders[0].notificationPhoneNumbers[0].areaCode.concat('-',sortingSearchResults.riders[0].notificationPhoneNumbers[0].phoneNumber)
      const secondPhoneNumber = sortingSearchResults.riders[1].notificationPhoneNumbers[0].areaCode.concat('-',sortingSearchResults.riders[1].notificationPhoneNumbers[0].phoneNumber)

      passengerColumns.ascSortLocator =  '.column-controls:nth(5) .fa-sort-asc'
      passengerColumns.descSortLocator = '.column-controls:nth(5) .fa-sort-desc'
      passengerColumns.columnLabelText = 'Phone Number'

      passengerColumns.firstRowActualDataLocator = 'td.passengerPhoneNumber > div:first'
      passengerColumns.lastRowActualDataLocator =  'td.passengerPhoneNumber > div:last'

      passengerColumns.firstAscRowExpectedData = firstPhoneNumber
      passengerColumns.lastAscRowExpectedData =  secondPhoneNumber
      passengerColumns.firstDescRowExpectedData = secondPhoneNumber
      passengerColumns.lastDescRowExpectedData = firstPhoneNumber

      onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
    })

     // DOB:
     it('Then Passenger Data should be sorted by DOB column in ascending and descending order.',()=>{
      passengerColumns.ascSortLocator =  '.column-controls:nth(6) .fa-sort-asc'
      passengerColumns.descSortLocator = '.column-controls:nth(6) .fa-sort-desc'
      passengerColumns.columnLabelText = 'DOB'

      passengerColumns.firstRowActualDataLocator = 'td.passengerDateOfBirth > div:first'
      passengerColumns.lastRowActualDataLocator =  'td.passengerDateOfBirth > div:last'

      passengerColumns.firstAscRowExpectedData = moment(sortingSearchResults.riders[0].dateOfBirth).format('MM/DD/yyyy')
      passengerColumns.lastAscRowExpectedData = moment(sortingSearchResults.riders[2].dateOfBirth).format('MM/DD/yyyy')
      passengerColumns.firstDescRowExpectedData = moment(sortingSearchResults.riders[2].dateOfBirth).format('MM/DD/yyyy')
      passengerColumns.lastDescRowExpectedData = moment(sortingSearchResults.riders[0].dateOfBirth).format('MM/DD/yyyy')

      onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
    })

    // Eligibility Start:
    it('Then Passenger Data should be sorted by Eligibility Start column in ascending and descending order.',()=>{
      passengerColumns.ascSortLocator =  '.column-controls:nth(7) .fa-sort-asc'
      passengerColumns.descSortLocator = '.column-controls:nth(7) .fa-sort-desc'
      passengerColumns.columnLabelText = 'Eligibility Start'

      passengerColumns.firstRowActualDataLocator = 'td.passengerEligibilityStart > div:first'
      passengerColumns.lastRowActualDataLocator =  'td.passengerEligibilityStart > div:last'

      passengerColumns.firstAscRowExpectedData = moment(sortingSearchResults.riders[0].categories[0].begin).format('MM/DD/yyyy')
      passengerColumns.lastAscRowExpectedData =  moment(sortingSearchResults.riders[2].categories[0].begin).format('MM/DD/yyyy')
      passengerColumns.firstDescRowExpectedData = moment(sortingSearchResults.riders[2].categories[0].begin).format('MM/DD/yyyy')
      passengerColumns.lastDescRowExpectedData = moment(sortingSearchResults.riders[0].categories[0].begin).format('MM/DD/yyyy')

      onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
    })

    // Eligibility End:
    it('Then Passenger Data should be sorted by Eligibility End column in ascending and descending order.',()=>{
        passengerColumns.ascSortLocator =  '.column-controls:nth(8) .fa-sort-asc'
        passengerColumns.descSortLocator = '.column-controls:nth(8) .fa-sort-desc'
        passengerColumns.columnLabelText = 'Eligibility End'

        passengerColumns.firstRowActualDataLocator = 'td.passengerEligibilityEnd > div:first'
        passengerColumns.lastRowActualDataLocator =  'td.passengerEligibilityEnd > div:last'

        passengerColumns.firstAscRowExpectedData = moment(sortingSearchResults.riders[0].categories[0].end).format('MM/DD/yyyy')
        passengerColumns.lastAscRowExpectedData =  moment(sortingSearchResults.riders[2].categories[0].end).format('MM/DD/yyyy')
        passengerColumns.firstDescRowExpectedData = moment(sortingSearchResults.riders[2].categories[0].end).format('MM/DD/yyyy')
        passengerColumns.lastDescRowExpectedData =  moment(sortingSearchResults.riders[0].categories[0].end).format('MM/DD/yyyy')

        onSearchPassengerSelectionPage.sortSearchPassenger(passengerColumns)
      })
   })

    afterEach(() => {
    })
})